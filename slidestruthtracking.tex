% SPDX-FileCopyrightText: 2022 Ricardo Guimarães
%
% SPDX-License-Identifier: Apache-2.0

%! TeX program = lualatex
\documentclass[UKenglish,xcolor=table]{beamer}
\setbeameroption{show notes}

\usetheme{metropolis}

\usepackage[UKenglish]{babel}
\usepackage[utf8]{luainputenc}
\usepackage{fontspec-luatex}
\usepackage{fontawesome}
%\usepackage[T1]{fontenc}
\usepackage{xparse}


\usepackage{array} % Tables + beamer overlay

\usepackage{tikz}
\usepackage{marvosym}
\usepackage{tikzsymbols}
\usepackage{smartdiagram}
\usepackage{pgfplots}
\usepgfplotslibrary{fillbetween}
\usepackage{ragged2e}
\usepackage{calc}
\usetikzlibrary{calc, positioning, shapes,
decorations,fit,intersections,patterns,arrows,shapes.arrows,
arrows.meta,trees,shapes.callouts, trees,shapes.symbols}
\usetikzlibrary{graphs,graphdrawing,quotes,graphs.standard}
\usetikzlibrary{overlay-beamer-styles}  % Overlay effects for TikZ
\usesmartdiagramlibrary{additions}
\usegdlibrary{force}
\usegdlibrary{layered}
\usegdlibrary{trees}
\usegdlibrary{circular}
\usetikzlibrary{mindmap}

\usepackage[draft]{tikzpeople}

\usepackage{booktabs}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{cleveref}

%\usepackage{siunitx} % automatic formatting of large numbers

\newtheorem{proposition}[theorem]{Proposition}

\usepackage{multirow}
\usepackage{xcolor}
\usepackage{appendixnumberbeamer}
\usepackage{colortbl}
\usepackage[normalem]{ulem} % strikeout text and the like
\usepackage{epstopdf} % Use eps images with Lualatex (for the logos)
% Break and typset long URLs:
%\usepackage[anythingbreaks]{breakurl}
\usepackage{xurl}

%\usepackage{bussproofs} % for typesetting proof trees

\usepackage[
  backend=biber,
  bibencoding=utf8,
  style=alphabetic
]{biblatex}
\addbibresource{slidestruthtracking.bib}
%\usepackage{natbib}
%\renewcommand{\bibsection}{\subsubsection*{\bibname{}}}

\usepackage{enumitem}
\setitemize{label=\usebeamerfont*{itemize item}%
  \usebeamercolor[fg]{itemize item}
  \usebeamertemplate{itemize item}
  }
\setlist{topsep=5pt,itemsep=6pt,partopsep=5pt, parsep=8pt}

\usepackage{subcaption} % Subfigures

%\usepackage{svg}

\usepackage{listings}
%\usepackage{minted}


%\usepackage{tikz-3dplot}

\usepackage{csquotes}       % Quotation marks
%\usepackage{microtype}      % Improved typography
\usepackage[absolute, overlay]{textpos} % Arbitrary placement
\setlength{\TPHorizModule}{\paperwidth} % Textpos units
\setlength{\TPVertModule}{\paperheight} % Textpos units

\usepackage{annotate-equations}


\definecolor{colorAO}{rgb}{0.0, 0.5, 0.0}
\definecolor{amber}{rgb}{1.0, 0.75, 0.0}
\definecolor{amethyst}{rgb}{0.6, 0.4, 0.8}
\definecolor{cherryblossompink}{rgb}{1.0, 0.72, 0.77}
\definecolor{darkelectricblue}{rgb}{0.33, 0.41, 0.47}
\definecolor{bittersweet}{rgb}{1.0, 0.44, 0.37}

\newcommand{\posmark}{\textcolor{colorAO}{$+$}}
\newcommand{\negmark}{\textcolor{red}{$-$}}
\newcommand{\mylink}[1]{{\textcolor{blue}{\underline{#1}}}}

\crefformat{footnote}{#2\footnotemark[#1]#3}


\graphicspath{{./images/}}

\setbeamercolor{palette retitle}{%
    use=normal text,
    fg=normal text.fg,
    bg=normal text.bg
}

\setbeamercolor{frametitle}{%
    use=palette retitle,
    parent=palette retitle
}

\setbeamertemplate{frametitle}{%
    \usebeamerfont{frametitle}\insertframetitle\strut%
    \vskip-.25\baselineskip%
    \color{orange}{
    \leaders\vrule width \paperwidth\vskip0.4pt%
}
    \vskip0pt%
    \nointerlineskip{}
}


\usepackage[mathrm=sym]{unicode-math}
\setmathfont{Latin Modern Math}
%\setmathfont{Computer Modern}
%\setmathfont{latinmodern-math.otf}
\usefonttheme{professionalfonts}

\AtBeginDocument{% to do this after unicode-math has done its work
  \renewcommand{\setminus}{\mathbin{\backslash}}%
}

%\tikzset{%
    %invisible/.style={opacity=0,text opacity=0},
    %visible on/.style={alt=#1{}{invisible}},
    %alt/.code args={<#1>#2#3}{%
        %\alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
    %},
%}


\input{commands.tex}

\author[Guimarães, R.]{%
    \textbf{Ricardo Guimarães} \textendash{}  University of Bergen
}
\date{07 October 2022}

\title{Truth-Tracking by Belief Revision}
\subtitle{\citeauthor{Baltag2018} (\citeyear{Baltag2018})}

\makeatletter
\patchcmd{\beamer@sectionintoc}
{\vskip1.5em}
  {\vskip0.5em}
  {}
  {}
\makeatother  




\tikzset{
  marked/.style={
    color=blue
  },
  err/.style={
    color=red
  },
  marked on/.style={alt=#1{marked}{}},
  marked2/.style={
    color=gray
  },
  marked2 on/.style={alt=#1{marked2}{}},
  marked3/.style={
      very thick,
      color=colorAO
  },
  marked4/.style={
      very thick,
      color=blue
  },
  marked4 on/.style={alt=#1{marked4}{}},
  err on/.style={alt=#1{err}{}}
}

\begin{document}


%\section{Overview}
%% Use
%%
%%     \begin{frame}[allowframebreaks]{Title}
%%
%% if the TOC does not fit one frame.
%\begin{frame}{Table of contents}
    %\tableofcontents[currentsection]
%\end{frame}

\begin{frame}[plain,noframenumbering]
    \titlepage{}
\end{frame}

\begin{frame}[plain,noframenumbering]{Outline}
    \tableofcontents[hideothersubsections]
\end{frame}

\input{sec_intro.tex}
\input{sec_beliefchange.tex}
\input{sec_learning.tex}
\input{sec_iterated.tex}

\section{Concluding Remarks}

\begin{frame}{Concluding Remarks}
    \begin{itemize}
        \item Connecting two different fields on AI
        \item AGM-like rational learners
        \item Different operators for different settings
        \item Other operators? Other logics?
        \item Polinomially learnable epistemic spaces?
    \end{itemize} 
\end{frame}

\begin{frame}[plain, noframenumbering]{Thank you!}

    {\Large{\textbf{Questions? Feedback?}}}

\end{frame}

\begin{frame}[plain, noframenumbering]{License}
    This presentation written by Ricardo Guimarães is licensed under
    the \href{https://creativecommons.org/licenses/by-sa/4.0/}{CC BY-SA 4.0}
\end{frame}


%\section{Highlighting}
%\SectionPage{}


%\begin{frame}{Highlighting}
    %Sometimes it is useful to \alert{highlight} certain words in the text.

    %\begin{alertblock}{Important message}
        %If a lot of text should be \alert{highlighted}, it is a good idea to put it in a box.
    %\end{alertblock}

    %It is easy to match the \structure{colour theme}.
%\end{frame}


%\section{Lists}


%\begin{frame}{Lists issasd d}
    %\begin{itemize}
        %\item
        %Bullet lists are marked with a red box.
    %\end{itemize}

    %\begin{enumerate}
        %\item
        %\label{enum:item}
        %Numbered lists are marked with a white number inside a red box.
    %\end{enumerate}

    %\begin{description}
        %\item[Description] highlights important words with red text.
    %\end{description}

    %Items in numbered lists like \enumref{enum:item} can be referenced with a red box.

    %\begin{example}
        %\begin{itemize}
            %\item
            %Lists change colour after the environment.
        %\end{itemize}
    %\end{example}
%\end{frame}


%\section{Effects}


%\begin{frame}{Effects}
    %\begin{columns}[onlytextwidth]
        %\begin{column}{0.49\textwidth}
            %\begin{enumerate}[<+-|alert@+>]
                %\item
                %Effects that control

                %\item
                %when text is displayed

                %\item
                %are specified with <> and a list of slides.
            %\end{enumerate}

            %\begin{theorem}<2>
                %This theorem is only visible on slide number 2.
            %\end{theorem}
        %\end{column}
        %\begin{column}{0.49\textwidth}
            %Use \textbf<2->{textblock} for arbitrary placement of objects.

            %\pause
            %\medskip

            %It creates a box
            %with the specified width (here in a percentage of the slide's width)
            %and upper left corner at the specified coordinate (x, y)
            %(here x is a percentage of width and y a percentage of height).
        %\end{column}
    %\end{columns}
    
    %\begin{textblock}{0.3}(0.45, 0.55)
        %\includegraphics<1, 3>[width = \textwidth]{UiB-images/UiB-emblem}
    %\end{textblock}
%\end{frame}


\section*{References}


\begin{frame}[allowframebreaks]{References}
        % Article is the default.
        %\bibliographystyle{alpha}
        %\AtNextBibliography{\tiny}
        \printbibliography[heading=none]
\end{frame}


\end{document}
