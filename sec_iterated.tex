% SPDX-FileCopyrightText: 2022 Ricardo Guimarães
%
% SPDX-License-Identifier: Apache-2.0

\section{Belief Revision-Based Learning}

\begin{frame}{Belief Revision Operators}

    \begin{align*}
        \eqnmark[darkelectricblue]{node1}{\orev}(\eqnmarkbox[bittersweet]{node2}{{\mathbb{B}_{\mathbb{S}}}}, \eqnmark[colorAO]{node3}{\modof{p}}) = \eqnmarkbox[olive]{node4}{{\pspc^\prime}}
    \end{align*}
    \annotate{below,left}{node1}{One-step revision operator}
    \annotate[yshift=4pt]{above,left}{node2}{Current epistemic state}
    \annotate[yshift=4pt]{above,right}{node3}{New information}
    \annotate{below,right}{node4}{New epistemic space}

    \begin{visibleenv}<2->
        \begin{align*}
            \eqnmark[darkelectricblue]{node5}{\irev}(\eqnmarkbox[bittersweet]{node6}{{\mathbb{B}_{\mathbb{S}}}}, \eqnmark[colorAO]{node7}{\sigma}) =
            \begin{cases}
                \pspc, &\text{ if } \sigma \text{ is empty;}\\
                \eqnmark[blue]{node8}{\orev}(\irev(\pspc, \sigma^\prime), \modof{p}), &\text{ if } \sigma = \sigma^\prime * \modof{p}
            \end{cases}
        \end{align*}

        \annotate[yshift=-6pt]{below,right}{node5}{Iterated\\\sffamily\footnotesize revision op.}
        \annotate[yshift=22pt]{above,right}{node6}{Current epistemic state}
        \annotate[yshift=8pt]{above,right}{node7}{New information}
        \annotate{below,right}{node8}{One-step revision op.}
    \end{visibleenv}
%\annotate{below,right}{node4}{probability that the\\\sffamily\footnotesize defender picks Tails}
\end{frame}

\begin{frame}{Conditioning}
    Let \(\pspc = (\pworlds, \obsp, \preceq)\) and \(\modof{p} \in \obsp\).
    \begin{align*}
        \Cond(\pspc, \modof{p}) \coloneqq (\pworlds \cap \modof{p}, \obsp, \preceq \cap (\pworlds^p \times \pworlds^p))
    \end{align*}
    \vspace{-0.5cm}
    Example:
    \begin{columns}[t]
             \column{0.5\textwidth}
        \begin{align*}
            \pworlds &= \left\{\modelQR, \modelQ, \modelPQ, \modelPQR \right\}\\ 
            \obsp &= \{ \modof{p}, \modof{q}\}\\
            \preceq &= 
        \end{align*}
        \begin{center}
            \input{expreorder2.tex} 
        \end{center}

        \column{0.5\textwidth}
        \begin{align*}
            \pworlds^p &= \left\{\modelPQ, \modelPQR \right\}\\ 
            \obsp &= \{ \modof{p}, \modof{q}\}\\
            \preceq^p &= 
        \end{align*}
        \begin{center}
            \input{expreorder_cond.tex} 
        \end{center}
    \end{columns}
\end{frame}

\note{Now \(Kp\) holds}

\begin{frame}{Lexicographic Revision}
    Let \(\pspc = (\pworlds, \obsp, \preceq)\) and \(\modof{p} \in \obsp\).
    \begin{align*}
        \Lex(\pspc, \modof{p}) &\coloneqq (\pworlds, \obsp, \preceq^\prime)\\
        \preceq^\prime &\coloneqq \left\{(\omega, \omega^\prime) \in \pworlds \times \pworlds \mid (\omega \preceq_p \omega^\prime) \text{ or } (\omega \preceq_{\bar{p}} \omega^\prime) \text{ or } \right. \\
        {} & \qquad\qquad\qquad\qquad\qquad \left. (\omega \in \modof{p} \text{ and } \omega^\prime \not\in \modof{p})\right\}\\
    \end{align*}
    \vspace{-1.5cm}
    \begin{columns}[t]
             \column{0.5\textwidth}
        \begin{align*}
            \pworlds &= \left\{\modelQR, \modelQ, \modelPQ, \modelPQR \right\}\\ 
            \obsp &= \{ \modof{p}, \modof{q}\}\\
            \preceq &= 
        \end{align*}
        \begin{center}
            \input{expreorder2.tex} 
        \end{center}

        \column{0.5\textwidth}
        \begin{align*}
            \preceq^\prime &= 
        \end{align*}
        \begin{center}
            \input{expreorder_lex.tex} 
        \end{center}
    \end{columns}
\end{frame}

\begin{frame}{Minimal Revision}
    Let \(\pspc = (\pworlds, \obsp, \preceq)\) and \(\modof{p} \in \obsp\).
    \begin{align*}
        \Mini(\pspc, \modof{p}) &\coloneqq (\pworlds, \obsp, \preceq^\prime)\\
        \preceq^\prime &\coloneqq \left\{(\omega, \omega^\prime) \in \pworlds \times \pworlds \mid (\omega \in \min_{\preceq_p} \text{ and }  \omega^\prime \not\in \min_{\preceq_p}), \right. \\  
        {} & \qquad\qquad\qquad\qquad \left. \text{ otherwise if } (\omega \preceq \omega^\prime) \right\} 
    \end{align*}
    \vspace{-1.5cm}
    \begin{columns}[t]
             \column{0.5\textwidth}
        \begin{align*}
            \pworlds &= \left\{\modelQR, \modelQ, \modelPQ, \modelPQR \right\}\\ 
            \obsp &= \{ \modof{p}, \modof{q}\}\\
            \preceq &= 
        \end{align*}
        \begin{center}
            \input{expreorder2.tex} 
        \end{center}

        \column{0.5\textwidth}
        \begin{align*}
            \preceq^\prime &= 
        \end{align*}
        \begin{center}
            \input{expreorder_mini.tex} 
        \end{center}
    \end{columns}
\end{frame}

\note{After the revision, \(Br\) holds}

\begin{frame}{Reminder}
    \begin{description}
        \item[Conditioning:] Only accept \(p\)-worlds
        \item[Lexicographic:] Prefer/Improve all \(p\)-worlds
        \item[Minimal:] Prefer/Improve the minimal \(p\)-worlds
     \end{description} 
\end{frame}

\begin{frame}{Prior Plausibility Assignment}

    \begin{description}
        \item[Epistemic Space:] \(\espc = (\pworlds, \obsp)\)
        \item[Plausibility Space:] \(\pspc = (\pworlds, \obsp, \preceq)\)
        \item[Problem:] we do not have \(\preceq\) defined a priori! 
    \end{description}

    \begin{visibleenv}<2->
        Then, let us pretend we do: 
        \begin{align*}
            \pplas((\pworlds, \obsp)) = \pspc = (S, O, \preceq) 
        \end{align*}
    \end{visibleenv}
\end{frame}

\begin{frame}{Belief Revision-Based Learning Method}
    \begin{align*}
        \eqnmark[darkelectricblue]{node1}{\learn}^{\pplas}_{\irev}(\eqnmarkbox[bittersweet]{node2}{\espc}, \eqnmark[colorAO]{node3}{\dseq}) \coloneqq \min \irev(\eqnmarkbox[blue]{node5}{\pplas(\espc)}, \sigma)
    \end{align*}
    %\eqnmark[darkelectricblue]{node1}{(-1)}
    %\annotate{below,left}{node2}{probability that the\\\sffamily\footnotesize defender picks Heads}
    \annotate{below,left}{node1}{Learning method}
    \annotate[yshift=4pt]{above,left}{node2}{Current (epistemic) state}
    \annotate[yshift=4pt]{above,right}{node3}{Data}
    \annotate{below,right}{node5}{Plausability space built\\\sffamily\footnotesize with prior plausibility}
\end{frame}

\begin{frame}{Properties of BRB Learners}
    % repeat the old ones and add the new from definition 14 
    \begin{itemize}
        \item BRB learning methods also can have the same properties 
        \item We say that a belief revision operator has a learning method property if the associated BRB has the property for any prior plausibility
    \end{itemize}

\end{frame}

\begin{frame}{Recap and More Properties of Learners}
    \begin{description}[nosep]
        \item[weakly retentive:] will entail the most recent data 
        \item[strongly retentive:] will entail all data seen 
        \item[conservative:] if the data was already entailed, keep the same beliefs 
        \item[strongly conservative:] if the data was already entailed, keep the same beliefs  and do not change the plausibility order
        \item[data-driven:]  conservative and weakly retentive
        \item[memory-free:] result only depends on the previous epistemic space and the new data
        \item[history-independent:] the output only depends on the previous plausibility space and the most recent observed data
    \end{description}
\end{frame}

\begin{frame}{Properties of BRB Learners: Example}
    \begin{itemize}
        \item \(\Cond\) is strongly retentive on sound data streams, not strongly conservative
        \item \(\Lex\) is strongly retentive on sound data streams (but not on arbitrary ones), not strongly conservative
        \item \(\Mini\) is not strongly retentive, but it is strongly conservative
    \end{itemize} 
\end{frame}

\begin{frame}{Identifiability in the Limit}
    Let \(\espc = (\pworlds, \obsp)\) and \(\omega \in \pworlds\)
    \begin{itemize}
        \item \(\learn\) identifies \(\omega\) in the limit: if for every sound and complete data stream w.r.t.\ \(\omega\), \(\learn\) outputs \(\omega\)
        \item Under good conditions the learner learns the world
        \item<2-> If it holds for all \(\omega \in \pworlds\), we say that \(\learn\) identifies \(\espc\) in the limit
        \item<2-> That is, \(\espc\) is learnable by \(\learn\)
    \end{itemize} 
\end{frame}

\begin{frame}{Universal Learning Methods}
    \begin{description}
        \item[Universal:] those that can identify in the limit any learnable \(\espc\) (in some class \(\lclass\))
        \item[Standardly Universal:] those that can identify in the limit any learnable \(\espc\) (in some class \(\lclass\)) with a well-founded\footnote{no infinitely descending chain of \(\prec\)} plausibility order
    \end{description}
\end{frame}

\begin{frame}{Main Results}
    There is an AGM-like universal belief revision method 

    Trick part: find the prior plausability order
    \begin{description}
        \item[Conditioning:] universal (the whole structure stabilises)
        \item[Lexicographic:] universal (only the minimal models stabilise)
        \item[Minimal:] it is not universal % consider showing the counter example
    \end{description}
\end{frame}

\begin{frame}{Enforce Well-foundedness}
    \begin{itemize}
        \item Assuming that AGM-like belief revision method generate a conservative BRB learner
        \item No conservative belief revision method generates a standardly universal BRB learners 
        \item We need non-well-founded plausibility orders!
    \end{itemize}
\end{frame}

\begin{frame}{Negation-closed Epistemic Spaces}
    \begin{description}
        \item[Positive observables:] \(\modof{p}, \modof{q}\) 
        \item[Negative observables:] \(\modof{\bar{p}}, \modof{\bar{q}}\) 
        \item[Negation-closed Epistemic Space:] \(\modof{p} \in \dstream\) iff \(\modof{\bar{p}} \in \dstream\)
    \end{description}
    \visible<2->{Remember: sound and complete data streams!}
\end{frame}

\begin{frame}{Positive/Negative Data}

    \begin{description}
        \item[Conditioning:] standardly universal
        \item[Lexicographic:] standardly universal
        \item[Minimal:] it is not universal % consider showing the counter example
    \end{description}
\end{frame}

\begin{frame}{Fair Data Streams}
    Dealing with unsound (noisy) data streams on negation-closed epistemic spaces
    \begin{description}
        \item[Fair data stream:] finite errors that are eventually corrected
            \begin{itemize}[nosep]
                \item The stream is complete for \(\omega \in \pworlds\) 
                \item There is \(n \in \mathbb{N}\) such that for all \(k \geq n\), \(\omega \in O_k\) 
                \item For every \(i \in \mathbb{N}\) such that \(\omega \not\in O_i\), there is some \(k > i\) with \(\omega \in O_k = \bar{O_i}\) 
            \end{itemize}
    \end{description} 
\end{frame}

\begin{frame}{Erroneous Information}
    \begin{description}
        \item[Conditioning:] not universal (assumes the input is always correct)
        \item[Lexicographic:] standardly universal
        \item[Minimal:] not universal
    \end{description}
\end{frame}
